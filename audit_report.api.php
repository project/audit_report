<?php

/**
 * @file
 * API documentation for the audit_report module.
 */

/**
 * @defgroup audit_check_plugin AuditCheck Plugin
 * @{
 * The audit check plugin does the work of evaluating potentially complex
 * compliance, data protection, security or other requirements.
 *
 * To define an audit check in a module you need to:
 * - Define a plugin by creating a new class that implements the
 *   \Drupal\audit_report\Plugin\AuditCheckInterface, in namespace
 *   Plugin\AuditCheck under your module namespace. For more information about
 *   creating plugins, see the @link plugin_api Plugin API topic. @endlink
 * - AuditCheck plugins use the annotations defined by
 *   \Drupal\audit_report\Annotation\AuditCheck. See the
 *   @link annotation Annotations topic @endlink for more information about
 *   annotations.
 * - \Drupal\audit_report\Plugin\AuditCheckBase provides convenience
 *   methods like ::addViolation, ::addMesage and ::buildResult. Usually you
 *   will want to extend this base class and implement ::getResult().
 * - Check results are cached, so make sure to specify the proper caching data.
 *
 * Things that make a great audit check:
 * - The result (value) is short and crisp, a single line if that.
 *
 *   Prefer short texts like "Incomplete" over "The extensions used by the
 *   module are incomplete". If necessary, add a longer explanatory text as
 *   the description.
 *
 * - In case of problems, provide direct help to fix them.
 *
 *   Provide links, menu locations, file paths or whatever helps the user to
 *   fix them. Example: "Please install <a href=":module">@module</a> module.".
 *
 * - When the underlying data changes, the audit result updates automatically.
 *
 *   You can ensure this by implementing ::getCacheTags and ::getCacheMaxAge,
 *   e.g. add the 'config:site.settings' tag for results depending on values
 *   of site.settings.
 *
 * @}
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allow modules to alter the audit check plugin definitions.
 *
 * @param array[] $definitions
 *   An array of audit check definitions, keyed by plugin ID.
 */
function hook_audit_report_info_alter(array &$definitions) {
  foreach ($definitions as $plugin_id => $definition) {
    // Add some tags for grouping.
    if (strpos($plugin_id, 'something_') === 0) {
      $definitions[$plugin_id]['tags'][] = t('Some tag');
    }
    // Remove audit checks that target 'some_module'.
    if (in_array('some_module', $definition['modules'])) {
      unset($definitions[$plugin_id]);
    }
  }
}

/**
 * @} End of "addtogroup hooks".
 */
