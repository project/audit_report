<?php

namespace Drupal\audit_report\Controller;

use Drupal\audit_report\Plugin\AuditCheckInterface;
use Drupal\audit_report\Plugin\AuditCheckManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for System Info routes.
 */
class AuditReport implements ContainerInjectionInterface {
  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The audit check manager.
   *
   * @var \Drupal\audit_report\Plugin\AuditCheckManager
   */
  protected $manager;

  /**
   * The cache backend for storing audit results.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $auditResultsCache;

  /**
   * Info array of supported severities.
   *
   * @var array
   */
  protected $severities;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.audit_check'),
      $container->get('cache.audit_report'),
      $container->get('date.formatter')
    );
  }

  /**
   * Constructs a AuditReportController object.
   *
   * @param \Drupal\audit_report\Plugin\AuditCheckManager $audit_check_manager
   *   The audit check manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend for storing audit results.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(AuditCheckManager $audit_check_manager, CacheBackendInterface $cache_backend, DateFormatterInterface $date_formatter) {
    $this->manager = $audit_check_manager;
    $this->auditResultsCache = $cache_backend;
    $this->dateFormatter = $date_formatter;

    $this->severities = self::getSeverities();
  }

  /**
   * Get the list of valid severities.
   */
  public static function getSeverities() {
    return [
      AuditCheckInterface::AUDIT_ERROR => [
        'title' => t('Errors found'),
        'status' => 'error',
      ],
      AuditCheckInterface::AUDIT_WARNING => [
        'title' => t('Warnings found'),
        'status' => 'warning',
      ],
      AuditCheckInterface::AUDIT_INFO => [
        'title' => t('Checked'),
        'status' => 'checked',
      ],
      AuditCheckInterface::AUDIT_OK => [
        'title' => t('OK'),
        // @todo Seven has no icon for "ok", only checkmark for "checked".
        'status' => 'checked',
      ],
    ];
  }

  /**
   * Builds a count of active audit plugins.
   *
   * @param int $active_checks
   *   The number of checks active on this page.
   * @param int $total_checks
   *   The total number of checks in the code base.
   *
   * @return array
   *   A render array for a counter of active audit checks.
   */
  protected function buildSummary($active_checks, $total_checks) {
    return [
      '#type' => 'inline_template',
      // @see status-report-counter.html.twig.
      '#template' => '<span class="system-status-counter system-status-counter--{{ severity }}">
        <span class="system-status-counter__status-icon system-status-counter__status-icon--{{ severity }}"></span>
        <span class="system-status-counter__status-title">
          <span class="system-status-counter__title">{{ title }}</span>
          {% if description %}<span class="system-status-counter__details">{{ description }}</span>{% endif %}
        </span>
      </span>',
      '#context' => [
        'title' => $this->formatPlural($active_checks,
          '@count of @total checks applicable',
          '@count of @total checks applicable', [
            '@total' => $total_checks,
          ]),
        'severity' => $active_checks == $total_checks ? 'checked' : 'warning',
      ],
      '#prefix' => '<br><div class="system-status-report-counters">',
      '#suffix' => '</div>' . ($active_checks > 0 ? '<br><hr><br>' : ''),
    ];
  }

  /**
   * Builds counters for each severity.
   *
   * @param array[] $results
   *   A list of audit results.
   *
   * @return array
   *   A render array for severity counters.
   */
  protected function buildSummaryCounters(array $results) {
    $build = [
      '#theme' => 'status_report_page',
    ];
    foreach ($this->getSummaryCounters($results) as $key => $counter) {
      if ($counter['amount'] === 0) {
        continue;
      }
      $text = $this->formatPlural($counter['amount'], $counter['text'], $counter['text_plural']);

      $build['#counters'][$key] = [
        '#theme' => 'status_report_counter',
        '#amount' => $counter['amount'],
        '#text' => $text,
        '#severity' => $key,
      ];
    }
    return $build;
  }

  /**
   * Determines the number of items with different severity for summary.
   */
  protected function getSummaryCounters(array $results) {
    $counters = [
      'error' => [
        'amount' => 0,
        'text' => t('Error'),
        'text_plural' => t('Errors'),
      ],
      'warning' => [
        'amount' => 0,
        'text' => t('Warning'),
        'text_plural' => t('Warnings'),
      ],
      'checked' => [
        'amount' => 0,
        'text' => t('Checked'),
        'text_plural' => t('Checked'),
      ],
    ];

    foreach ($results as $plugin_id => $result) {
      $severity = $this->severities[AuditCheckInterface::AUDIT_INFO];
      if (isset($result['severity'])) {
        $severity = $this->severities[(int) $result['severity']];
      }
      if (isset($counters[$severity['status']])) {
        $counters[$severity['status']]['amount']++;
      }
    }
    return $counters;
  }

  /**
   * Title callback for report pages.
   */
  public function title($group = NULL) {
    if ($group) {
      $label = $this->manager->getAllTags()[$group] ?? '';
      return $this->t('@group audit report', ['@group' => $label]);
    }
    return $this->t('Site audit report');
  }

  /**
   * Displays a report of all audit check results.
   *
   * @param string $group
   *   An optional group name to limit results.
   *
   * @return array
   *   A render array with audit results, grouped by severity.
   */
  public function report($group = NULL) {
    $plugins = $this->manager->getActivePlugins($group ? $group : NULL);
    if (empty($plugins)) {
      throw new NotFoundHttpException('There are no active audit checks in this group.');
    }
    $results = $this->manager->executeMultiple($plugins);
    // Used to determine total number of available checks.
    $definitions = $this->manager->getDefinitions();
    if ($group) {
      $tag = $this->manager->getAllTags()[$group];
      $definitions = array_filter($definitions, function ($definition) use ($tag) {
        return in_array($tag, $definition['tags']);
      });
    }

    // Add audit summary.
    $build['audits'] = $this->buildSummary(count($plugins), count($definitions));
    $build['report'] = $this->buildSummaryCounters($results);

    // Sort audits by descending result severity.
    uasort($results, function ($a, $b) {
      if ($a['severity'] !== $b['severity']) {
        return -1 * ($a['severity'] <=> $b['severity']);
      }
      elseif ((string) $a['category'] !== (string) $b['category']) {
        return strnatcasecmp($a['category'], $b['category']);
      }
      return strnatcasecmp($a['title'], $b['title']);
    });

    $headers = [
      'title' => [
        'data' => $this->t('Title'),
        'width' => '180px',
      ],
      'result' => ['data' => $this->t('Result')],
      'metadata' => [
        'data' => $this->t('Last checked'),
        'width' => '180px',
      ],
    ];
    $rows = [];
    foreach ($results as $plugin_id => $result) {
      $result = [
        'plugin' => $plugins[$plugin_id],
      ] + $results[$plugin_id];
      $row = $this->buildRow($result);
      $rows[$result['severity']][$plugin_id] = $row;
    }
    foreach ($this->severities as $key => $severity) {
      if (empty($rows[$key])) {
        continue;
      }
      $build['severities'][$key] = [
        '#type' => 'table',
        '#header' => $headers,
        '#rows' => $rows[$key],
        '#attributes' => [
          'id' => $severity['status'],
        ],
        '#prefix' => '<h2>' . $severity['title'] . '</h2>',
        '#suffix' => '<br>',
      ];
    }

    return $build;
  }

  /**
   * Builds a table row for a single audit result.
   *
   * @param array $result
   *   The audit result.
   *
   * @return array
   *   A render array suitable for table #rows.
   */
  protected function buildRow(array $result) {
    $severity = $this->severities[$result['severity'] ?? AuditCheckInterface::AUDIT_INFO];
    $is_ignored = !empty($result['ignored']);

    $row = [];
    $row['title'] = [
      'data' => [
        'title' => ['#markup' => $result['title']],
      ],
      'class' => [
        'system-status-report__status-title',
        'system-status-report__status-icon',
        'system-status-report__status-icon--' . ($is_ignored ? 'warning' : $severity['status']),
      ],
      'title' => $severity['title'],
    ];
    $row['result'] = [
      'data' => [
        '#type' => 'inline_template',
        '#template' => '<span class="title">{{ value }}</span>{% if description %}<div class="description">{{ description }}</div>{% endif %}',
        '#context' => [
          'value' => $result['value'],
          'description' => isset($result['description']) ? $result['description'] : NULL,
        ],
      ],
      'style' => 'vertical-align: top;',
    ];
    $row['metadata'] = [
      'data' => [
        'timestamp' => [
          '#plain_text' => $this->dateFormatter->format($result['cache']['created']),
        ],
      ],
      'style' => 'vertical-align: top;',
    ];
    if ($category = $result['plugin']->getCategory()) {
      $row['metadata']['data']['category'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('@group: @title', [
          '@group' => $this->t('Category'),
          '@title' => $category,
        ]),
      ];
    }
    if ($tags = $result['plugin']->getTags()) {
      $tag_links = [];
      foreach ($tags as $machine_name => $label) {
        $url = Url::fromRoute('audit_report.group_report', [
          'group' => $machine_name,
        ]);
        $tag_links[] = Link::fromTextAndUrl($label, $url)->toString();
      }
      $row['metadata']['data']['tags'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('@group: @title', [
          '@group' => $this->t('Tags'),
          // Links are already filtered using Link::toString.
          '@title' => Markup::create(implode(', ', $tag_links)),
        ]),
      ];
    }

    if ($is_ignored) {
      $row['title']['data']['ignored'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="description">{{ message }}</div>',
        '#context' => [
          'message' => !empty($result['ignored']['temporary']) ? $this->t('This check is ignored.') : $this->t('This check is ignored, it will be re-enabled when the result changes.'),
        ],
      ];
      $row['title']['data']['unignore'] = [
        '#type' => 'link',
        '#title' => $this->t('Enable'),
        '#url' => Url::fromRoute('audit_report.plugin.enable', [
          'plugin_id' => $result['plugin']->getPluginId(),
        ]),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
      $row['metadata']['data']['ignored'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="description"><p>{{ reason|nl2br }}</p></div>',
        '#context' => [
          'reason' => $result['ignored']['reason'],
        ],
        '#weight' => 9,
      ];
    }
    elseif ($result['severity'] != AuditCheckInterface::AUDIT_OK) {
      $row['title']['data']['ignore'] = [
        '#type' => 'link',
        '#title' => $this->t('Ignore'),
        '#url' => Url::fromRoute('audit_report.plugin.disable', [
          'plugin_id' => $result['plugin']->getPluginId(),
        ]),
        '#attributes' => [
          'class' => 'use-ajax',
          'data-dialog-renderer' => 'off_canvas',
          'data-dialog-type' => 'dialog',
          'data-dialog-options' => '{"width":"30%"}',
        ],
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
    }

    return [
      'data' => $row,
      'class' => [
        'system-status-report__entry',
        'system-status-report__entry--' . $severity['status'],
        'color-' . $severity['status'],
      ],
    ];
  }

}
