<?php

namespace Drupal\audit_report\Controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\State;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides callbacks for audit_report menu items.
 */
class AuditCallbacks implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * The cache backend for storing audit results.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.audit_report'),
      $container->get('request_stack'),
      $container->get('messenger'),
      $container->get('state')
    );
  }

  /**
   * Constructs a AuditReportController object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend storing the audit results.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\State\State $state
   *   The state service.
   */
  public function __construct(CacheBackendInterface $cache, RequestStack $request_stack, MessengerInterface $messenger, State $state) {
    $this->cache = $cache;
    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
    $this->state = $state;
  }

  /**
   * Re-enables a previously ignored audit check.
   *
   * @param string $plugin_id
   *   The audit check plugin being acted upon.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect back to the report page.
   */
  public function enable($plugin_id) {
    $ignored_plugins = $this->state->get('audit_report.ignored_plugins', []);
    unset($ignored_plugins[$plugin_id]);
    $this->state->set('audit_report.ignored_plugins', $ignored_plugins);

    $url = Url::fromRoute('audit_report.report', [], ['absolute' => TRUE])
      ->toString();
    return new RedirectResponse($url, 302);
  }

  /**
   * Flushes audit report cache.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect response redirecting to previous page or front page.
   */
  public function flushCache() {
    $this->cache->deleteAll();
    $this->messenger->addMessage($this->t('Audit report cache cleared.'));
    return new RedirectResponse($this->getPreviousPageUrl());
  }

  /**
   * Gets URL of previous page.
   *
   * @return string
   *   Value of HTTP REFERRER of current request or '/'.
   */
  public function getPreviousPageUrl() {
    $request = $this->requestStack->getCurrentRequest();
    if ($request->server->get('HTTP_REFERER')) {
      return $request->server->get('HTTP_REFERER');
    }
    else {
      return '/';
    }
  }

}
