<?php

namespace Drupal\audit_report;

/**
 * Defines events for audit checks.
 */
final class AuditEvents {

  /**
   * The name of the event fired when the result of an audit check changes.
   *
   * This event allows you to perform custom actions whenever the result of an
   * audit check changes.
   *
   * @Event
   */
  const RESULT_CHANGED = 'audit_report.result_changed';

}
