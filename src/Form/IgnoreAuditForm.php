<?php

namespace Drupal\audit_report\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\State;
use Drupal\Core\Url;
use Drupal\audit_report\Plugin\AuditCheckManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class IgnoreAuditForm extends ConfirmFormBase {

  /**
   * The audit check manager.
   *
   * @var \Drupal\audit_report\Plugin\AuditCheckManager
   */
  protected $auditCheckManager;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Audit plugin id to ignore.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * Constructs a new IgnoreAuditForm instance.
   *
   * @param \Drupal\audit_report\Plugin\AuditCheckManager $auditCheckManager
   *   The audit check manager.
   * @param \Drupal\Core\State\State $state
   *   The state service.
   */
  public function __construct(AuditCheckManager $auditCheckManager, State $state) {
    $this->auditCheckManager = $auditCheckManager;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.audit_check'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $plugin_id = NULL, string $result_hash = NULL) {
    $this->pluginId = $plugin_id;
    $form = parent::buildForm($form, $form_state);
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#required' => TRUE,
    ];
    $form['approved_by'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Approved by'),
      '#description' => $this->t('Name of the senior developer you consulted for approval.'),
      '#required' => TRUE,
    ];
    $form['reason_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Reason'),
      '#description' => $this->t('Please specify why this audit check result can be ignored.'),
      '#required' => TRUE,
    ];
    $form['ignore_always_pre'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore forever'),
      '#description' => $this->t('If set, this audit check will stay ignored even if the result changes later.'),
    ];
    $form['advanced']['ignore_always'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I am absolutely sure.'),
      '#states' => [
        'visible' => [
          ':input[name="ignore_always_pre"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $name = $form_state->getValue('name');
    $senior = $form_state->getValue('approved_by');
    $date = date('Y-m-d H:i:s');
    $reason = sprintf("%s\n\nName: %s\nSenior: %s\nDate: %s", $form_state->getValue('reason_text'), $name, $senior, date('Y-m-d H:i:s'));

    $ignored_plugins = $this->state->get('audit_report.ignored_plugins', []);
    $ignored_plugins[$this->pluginId] = [
      'reason' => $reason,
      'temporary' => !$form_state->getValue('ignore_always'),
    ];
    $this->state->set('audit_report.ignored_plugins', $ignored_plugins);

    $form_state->setRedirect('audit_report.report');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'audit_report_ignore_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('audit_report.report');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $definition = $this->auditCheckManager->getDefinition($this->pluginId);
    $plugin = $this->auditCheckManager->createInstance($this->pluginId, $definition);
    return $this->t('Ignore %plugin_label audit check?', [
      '%plugin_label' => $plugin->getLabel(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $definition = $this->auditCheckManager->getDefinition($this->pluginId);
    $plugin = $this->auditCheckManager->createInstance($this->pluginId, $definition);
    return $this->t('Do you really want to ignore the result of the %plugin_label audit check?', [
      '%plugin_label' => $plugin->getLabel(),
    ]);
  }

}
