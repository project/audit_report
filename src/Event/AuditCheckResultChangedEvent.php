<?php

namespace Drupal\audit_report\Event;

use Drupal\audit_report\Plugin\AuditCheckInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Events that are fired in relation to audit checks.
 */
class AuditCheckResultChangedEvent extends Event {

  /**
   * The audit check plugin.
   *
   * @var \Drupal\audit_report\Plugin\AuditCheckInterface
   */
  protected $auditPlugin;

  /**
   * The hash of the current audit check result.
   *
   * @var string
   */
  protected $hash = NULL;

  /**
   * The hash of the original audit check result.
   *
   * @var string
   */
  protected $originalHash = NULL;

  /**
   * The current audit check result.
   *
   * @var array
   */
  protected $result = NULL;

  /**
   * Constructs the event object.
   *
   * @param \Drupal\audit_report\Plugin\AuditCheckInterface $audit_plugin
   *   The account of the user logged in.
   * @param array $result
   *   The current audit check result of the given plugin.
   * @param string $original_hash
   *   Hash value of the audit check result before the change.
   * @param string $current_hash
   *   Hash value of the audit check result after the change.
   */
  public function __construct(AuditCheckInterface $audit_plugin, array $result, string $original_hash, string $current_hash) {
    $this->auditPlugin = $audit_plugin;
    $this->result = $result;
    $this->originalHash = $original_hash;
    $this->hash = $current_hash;
  }

  /**
   * Retrieves the severity value from an audit check result hash.
   *
   * @param string $hash
   *   The hash to operate on.
   *
   * @return int
   *   The severity contained in the hash.
   */
  protected function extractSeverityFromHash(string $hash) {
    return (int) explode(':', $hash)[0];
  }

  /**
   * Gets the current audit check result severity.
   *
   * @return int
   *   The current severity of the audit check result.
   */
  public function getSeverity() {
    return $this->extractSeverityFromHash($this->hash);
  }

  /**
   * Gets the original audit check result severity.
   *
   * @return int
   *   The severity of the audit check result before the change.
   */
  public function getOriginalSeverity() {
    return $this->extractSeverityFromHash($this->originalHash);
  }

  /**
   * Determines whether the check's severity has changed.
   *
   * New audit checks will always report as changed.
   *
   * @return bool
   *   True if the severity has changes since last time.
   */
  public function hasSeverityChanged() {
    return empty($this->originalHash) || $this->getSeverity() !== $this->getOriginalSeverity();
  }

  /**
   * Determines whether the check's value or description has changed.
   *
   * New audit checks will always report as changed.
   *
   * @return bool
   *   True if the severity has changes since last time.
   */
  public function hasContentChanged() {
    return empty($this->originalHash) || substr($this->hash, 2) !== substr($this->originalHash, 2);
  }

  /**
   * Gets the affected audit plugin.
   *
   * @return \Drupal\audit_report\Plugin\AuditCheckInterface
   *   The audit plugin.
   */
  public function getPlugin() {
    return $this->auditPlugin;
  }

  /**
   * Gets the current result of the audit plugin.
   *
   * @return array
   *   The result array.
   */
  public function getResult() {
    return $this->result;
  }

  /**
   * Gets the current result's hash.
   *
   * @return string
   *   The result hash.
   */
  public function getHash() {
    return $this->hash;
  }

  /**
   * Gets the original result's hash.
   *
   * @return string
   *   The result hash.
   */
  public function getOriginalHash() {
    return $this->originalHash;
  }

}
