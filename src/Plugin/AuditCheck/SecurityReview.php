<?php

namespace Drupal\audit_report\Plugin\AuditCheck;

use Drupal\audit_report\Plugin\AuditCheckBase;
use Drupal\audit_report\Plugin\AuditCheckInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\security_review\Checklist;
use Drupal\security_review\CheckResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an audit check for security_review module.
 *
 * @AuditCheck(
 *   id = "security_review",
 *   label = @Translation("Security review"),
 *   category = @Translation("Security Review"),
 *   modules = {
 *     "security_review",
 *   },
 *   tags = {
 *     @Translation("Security Review"),
 *   },
 *   deriver = "Drupal\audit_report\Plugin\Derivative\SecurityReviewCheckDeriver",
 * )
 */
class SecurityReview extends AuditCheckBase implements ContainerFactoryPluginInterface {

  /**
   * The security review check plugin.
   *
   * @var \Drupal\security_review\Check
   */
  protected $check;

  /**
   * Maps security_review result to severity.
   *
   * @var array
   *   Severities keyed by security_review result code.
   */
  protected static $severityMap = [
    CheckResult::SUCCESS => AuditCheckInterface::AUDIT_OK,
    CheckResult::FAIL => AuditCheckInterface::AUDIT_ERROR,
    CheckResult::WARN => AuditCheckInterface::AUDIT_WARNING,
    CheckResult::INFO => AuditCheckInterface::AUDIT_INFO,
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Checklist $checklist) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->check = $checklist->getCheckById($this->getDerivativeId());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('security_review.checklist')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return !$this->check->isSkipped()
      && \Drupal::currentUser()->hasPermission('run security checks');
  }

  /**
   * {@inheritdoc}
   */
  public function getResult() {
    $result_info = $this->check->run();
    $result_code = $result_info->result();
    $findings = $result_info->findings();
    if (($total_findings = count($findings)) > 10) {
      // Some checks have way too many violations, do not bloat.
      // @todo Lead the user to the full result list instead.
      $findings = array_slice($findings, 0, 10);
      $findings[] = $this->formatPlural(number_format($total_findings - 10),
        'There is @count additional result.',
        'There are @count additional results.'
      );
    }

    $check_result = $this->check->createResult($result_code, $findings, $result_info->isVisible(), $result_info->time());

    return [
      'severity' => static::$severityMap[$result_code],
      'value' => $this->check->getMessage($result_code),
      'description' => $this->check->evaluate($check_result),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $this->addCacheTags([
      'config:security_review.settings',
      'config:security_review.check.' . $this->getDerivativeId(),
    ]);
    return parent::getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // We have no detailed caching information, but running these checks is
    // expensive. Thus, do not recompute too often.
    return 24 * 60 * 60;
  }

}
