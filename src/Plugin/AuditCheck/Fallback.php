<?php

namespace Drupal\audit_report\Plugin\AuditCheck;

use Drupal\audit_report\Plugin\AuditCheckBase;
use Drupal\audit_report\Plugin\AuditCheckInterface;

/**
 * Provides a fallback audit plugin.
 *
 * @AuditCheck(
 *   id = "audit_fallback",
 *   label = @Translation("Audit Fallback Plugin"),
 * )
 */
class Fallback extends AuditCheckBase {

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getResult() {
    return [
      'severity' => AuditCheckInterface::AUDIT_INFO,
      'value' => $this->t('Undefined.'),
      'description' => $this->t('This value has been provided by a fallback plugin on behalf of a plugin that has since been removed.'),
    ];
  }

}
