<?php

namespace Drupal\audit_report\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Executable\ExecutableInterface;
use Drupal\Core\Executable\ExecutableManagerInterface;

/**
 * Defines an interface for Audit check plugins.
 */
interface AuditCheckInterface extends ExecutableInterface, PluginInspectionInterface, RefinableCacheableDependencyInterface {

  const AUDIT_ERROR = 2;
  const AUDIT_WARNING = 1;
  const AUDIT_OK = 0;
  const AUDIT_INFO = -1;

  /**
   * Sets the executable manager class.
   *
   * @param \Drupal\Core\Executable\ExecutableManagerInterface $executableManager
   *   The executable manager.
   */
  public function setExecutableManager(ExecutableManagerInterface $executableManager);

  /**
   * Determines whether a plugin should be executed for this site.
   *
   * @return bool
   *   TRUE if the audit check defined by this plugin should be executed.
   */
  public function isActive();

  /**
   * Get the human readable label of the audit check.
   */
  public function getLabel();

  /**
   * Get the human readable category of the audit check.
   */
  public function getCategory();

  /**
   * Get associated tags.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   The tags.
   */
  public function getTags();

  /**
   * Gets the result of the audit check.
   *
   * @return array
   *   An array containing the following keys:
   *   - severity: One of the AuditCheckInterface::AUDIT_* constants.
   *   - value: Short label describing current audit status.
   *   - description: Long description of the current audit status,
   *     can be a string or a renderable.
   */
  public function getResult();

}
