<?php

namespace Drupal\audit_report\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Audit check plugins.
 *
 * @todo Cache contexts are currently unsupported!
 */
abstract class AuditCheckBase extends PluginBase implements AuditCheckInterface, ContainerFactoryPluginInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * The audit check plugin manager to proxy execute calls through.
   *
   * @var \Drupal\Core\Executable\ExecutableManagerInterface
   */
  protected $executableManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The system environment name.
   *
   * @var string
   */
  protected $environment;

  /**
   * The severity of this audit's result.
   *
   * @var int
   */
  protected $severity = AuditCheckInterface::AUDIT_OK;

  /**
   * Violations found by the audit check.
   *
   * @var array
   *   A list of renderables.
   */
  protected $violations = [];

  /**
   * Messages from the audit check.
   *
   * @var array
   *   A list of renderables.
   */
  protected $messages = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    return $this->executableManager->execute($this);
  }

  /**
   * {@inheritdoc}
   */
  public function setExecutableManager(ExecutableManagerInterface $executableManager) {
    $this->executableManager = $executableManager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    if (!empty($this->pluginDefinition['modules'])) {
      $this->addCacheTags(['config:core.extension']);
    }
    return $this->cacheTags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return CacheBackendInterface::CACHE_PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return isset($this->pluginDefinition['label']) ? $this->pluginDefinition['label'] : $this->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getCategory() {
    return isset($this->pluginDefinition['category']) ? $this->pluginDefinition['category'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    $tags = $this->pluginDefinition['tags'];
    return array_intersect($this->executableManager->getAllTags(), $tags);
  }

  /**
   * Add a violation and update severity accordingly.
   *
   * @param mixed $message
   *   A renderable that explains what should be done.
   * @param int $severity
   *   The severity of this violation.
   */
  protected function addViolation($message, $severity) {
    if ($severity === AuditCheckInterface::AUDIT_OK) {
      $this->addMessage($message);
    }
    $this->violations[$severity][] = $message;
    $this->severity = max($this->severity, $severity);
  }

  /**
   * Add a message to the audit result.
   *
   * @param mixed $message
   *   The renderable message.
   */
  protected function addMessage($message) {
    $this->messages[] = $message;
  }

  /**
   * {@inheritdoc}
   *
   * Child plugins can simply perform their evaluation, add violations when
   * they arise and call parent::buildResult() for a default result summary
   * to get a data structure suitable for ::getResult.
   */
  public function buildResult() {
    $result = [
      'severity' => $this->severity,
    ];
    switch ($this->severity) {
      case AuditCheckInterface::AUDIT_ERROR:
        $result['value'] = $this->t('Invalid');
        break;

      case AuditCheckInterface::AUDIT_WARNING:
        $result['value'] = $this->t('Misconfigured');
        break;

      case AuditCheckInterface::AUDIT_INFO:
      case AuditCheckInterface::AUDIT_OK:
        $result['value'] = $this->t('OK');
        break;

      default:
        $result['value'] = $this->t('Unknown');
        break;
    }

    // Prepare flat list of violations ordered by severity descending.
    krsort($this->violations);
    $violations = array_reduce($this->violations, function (array $carry, array $item) {
      return array_merge($carry, $item);
    }, []);

    if ($this->severity > AuditCheckInterface::AUDIT_OK) {
      $result['value'] = $this->formatPlural(count($violations),
        '@label (@count violation)',
        '@label (@count violations)', [
          '@label' => $result['value'],
        ]);
    }

    if ($violations) {
      $result['description']['violations'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $violations,
        '#wrapper_attributes' => [
          'class' => ['container', 'violations'],
        ],
      ];
    }
    if ($this->messages) {
      $result['description']['messages'] = [
        '#type' => 'inline_template',
        '#template' => '<p class="description">{{ messages|safe_join("<br>") }}</p>',
        '#context' => [
          'messages' => $this->messages,
        ],
      ];
    }

    return $result;
  }

  /**
   * Gets the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  protected function getModuleHandler() {
    if (!isset($this->moduleHandler)) {
      $this->moduleHandler = \Drupal::moduleHandler();
    }
    return $this->moduleHandler;
  }

  /**
   * Gets the config factory.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The config factory.
   */
  protected function getConfigFactory() {
    if (!isset($this->configFactory)) {
      $this->configFactory = \Drupal::configFactory();
    }
    return $this->configFactory;
  }

  /**
   * Gets the date formatter.
   *
   * @return \Drupal\Core\Datetime\DateFormatterInterface
   *   The date formatter.
   */
  protected function getDateFormatter() {
    if (!isset($this->dateFormatter)) {
      $this->dateFormatter = \Drupal::service('date.formatter');
    }
    return $this->dateFormatter;
  }

  /**
   * Gets the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected function getEntityTypeManager() {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }
    return $this->entityTypeManager;
  }

  /**
   * Gets the config factory.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The config factory.
   */
  protected function getDatabase() {
    if (!isset($this->database)) {
      $this->database = \Drupal::database();
    }
    return $this->database;
  }

  /**
   * Generates a label for a given interval.
   *
   * @param int $interval
   *   The interval in seconds.
   *
   * @return string
   *   The generated label.
   */
  protected function getIntervalLabel($interval) {
    if ($interval === 0) {
      return $this->t('Never');
    }

    return $this->getDateFormatter()->formatInterval($interval);
  }

}
