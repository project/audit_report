<?php

namespace Drupal\audit_report\Plugin;

use Drupal\Component\Plugin\CategorizingPluginManagerInterface;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Executable\ExecutableException;
use Drupal\Core\Executable\ExecutableInterface;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\CategorizingPluginManagerTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\State\State;
use Drupal\audit_report\AuditEvents;
use Drupal\audit_report\Event\AuditCheckResultChangedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides the Audit check plugin manager.
 */
class AuditCheckManager extends DefaultPluginManager implements ExecutableManagerInterface, CategorizingPluginManagerInterface, FallbackPluginManagerInterface {

  use CategorizingPluginManagerTrait;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The cache backend for storing audit results.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The cache contexts manager.
   *
   * @var \Drupal\Core\Cache\Context\CacheContextsManager
   */
  protected $cacheContextsManager;

  /**
   * {@inheritdoc}
   */
  protected $defaults = [
    'modules' => [],
    'tags' => [],
  ];

  /**
   * Constructs a new AuditCheckManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\State\State $state
   *   The state service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Cache\CacheBackendInterface $result_cache
   *   The cache backend for storing audit results.
   * @param \Drupal\Core\Cache\Context\CacheContextsManager $cache_contexts_manager
   *   The cache contexts manager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, State $state, EventDispatcherInterface $event_dispatcher, CacheBackendInterface $result_cache, CacheContextsManager $cache_contexts_manager) {
    parent::__construct(
      'Plugin/AuditCheck',
      $namespaces,
      $module_handler,
      'Drupal\audit_report\Plugin\AuditCheckInterface',
      'Drupal\audit_report\Annotation\AuditCheck'
    );

    $this->alterInfo('audit_report_info');
    $this->setCacheBackend($cache_backend, 'audit_report_plugins');

    $this->state = $state;
    $this->eventDispatcher = $event_dispatcher;
    $this->cache = $result_cache;
    $this->cacheContextsManager = $cache_contexts_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $plugin = parent::createInstance($plugin_id, $configuration);
    return $plugin->setExecutableManager($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'audit_fallback';
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);
    $this->processDefinitionCategory($definition);
  }

  /**
   * Get a list of tags used by audit check plugins.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   The list of tag labels, keyed by machine name, sorted alphabetically.
   *
   * @todo Cache this list.
   */
  public function getAllTags() {
    $tags = [];
    // @todo Use dependency injection or find a cleaner solution.
    $transliteration = \Drupal::transliteration();
    foreach ($this->getDefinitions() as $definition) {
      foreach ($definition['tags'] as $label) {
        $machine_name = mb_strtolower($label->getUntranslatedString());
        $machine_name = $transliteration->transliterate($machine_name, 'en', '-');
        $machine_name = preg_replace('/[^a-z0-9-]+/', '-', $machine_name);
        $machine_name = preg_replace('/\-+/', '-', $machine_name);
        $tags[$machine_name] = $label;
      }
    }
    natcasesort($tags);
    return $tags;
  }

  /**
   * Gets all AuditCheck plugins that are currently active.
   *
   * @param string $tag
   *   A tag machine name to filter plugins by.
   *
   * @return array
   *   An array of active plugin instances, keyed by plugin id.
   */
  public function getActivePlugins($tag = NULL) {
    $plugins = [];

    $definitions = $this->getDefinitions();
    if (!is_null($tag)) {
      // We need a translatable string for comparison.
      $tag = (string) $this->getAllTags()[$tag];
    }
    foreach ($definitions as $plugin_id => $definition) {
      if ($tag && !in_array($tag, array_map('strval', $definition['tags']))) {
        // Filter by tags.
        continue;
      }
      // Check if all required modules are enabled.
      foreach ($definition['modules'] ?? [] as $module_name) {
        if (!$this->providerExists($module_name)) {
          continue 2;
        }
      }

      // Add active plugins to result set.
      $plugin = $this->createInstance($plugin_id, $definition);
      if ($plugin->isActive()) {
        $plugins[$plugin_id] = $plugin;
      }
    }

    return $plugins;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   An audit result containing the following keys:
   *   - label: The label auf the audit check.
   *   - category: The category of the audit check.
   *   - severity: One of the AuditCheckInterface::AUDIT_* constants.
   *   - value: Short label describing current audit status.
   *   - description: Long description of the current audit status.
   *   - ignored: If the check is ignored an array of details, or null.
   */
  public function execute(ExecutableInterface $plugin) {
    if (!$plugin instanceof AuditCheckInterface) {
      throw new ExecutableException("This manager object can only execute audit check plugins");
    }

    $plugin_id = $plugin->getPluginId();
    $cid = 'audit_result:' . $plugin_id;
    if ($cache = $this->cache->get($cid)) {
      $result = $cache->data;

      $result['cache']['created'] = $cache->created;
      $result['cache']['expire'] = $cache->expire;
    }
    else {
      // We can't call ::execute because of recursion.
      $result = $plugin->getResult();

      $created = time();
      $max_age = $plugin->getCacheMaxAge();
      $tags = $plugin->getCacheTags();
      if ($contexts = $plugin->getCacheContexts()) {
        foreach ($this->cacheContextsManager->parseTokens($contexts) as list($context_id, $parameter)) {
          // CacheContextsManager::getService method is protected.
          $metadata = \Drupal::service('cache_context.' . $context_id)
            ->getCacheableMetadata($parameter);
          $max_age = Cache::mergeMaxAges($max_age, $metadata->getCacheMaxAge());
          $tags = Cache::mergeTags($tags, $metadata->getCacheTags());
        }
      }
      $expires = $max_age == Cache::PERMANENT ? $max_age : $created + $max_age;
      $this->cache->set($cid, $result, $expires, $tags);

      $result['cache']['created'] = $created;
      $result['cache']['expire'] = $expires;
    }

    // Merge basic plugin info.
    $result['title'] = $plugin->getLabel();
    $result['category'] = $plugin->getCategory();
    $result['hash'] = $this->generateResultHash($result);

    // Check if result has changed since the plugin was ignored.
    $stored_hashes = $this->state->get('audit_report.plugin_result_hashes', []);
    $ignored_plugins = $this->state->get('audit_report.ignored_plugins', []);
    if (!isset($stored_hashes[$plugin_id]) || $stored_hashes[$plugin_id] != $result['hash']) {
      // Dispatch hash changed event.
      $original_hash = $stored_hashes[$plugin_id] ?? '';
      $event = new AuditCheckResultChangedEvent($plugin, $result, $original_hash, $result['hash']);
      $this->eventDispatcher->dispatch(AuditEvents::RESULT_CHANGED, $event);

      // Update plugin result hash.
      $stored_hashes[$plugin_id] = $result['hash'];
      $this->state->set('audit_report.plugin_result_hashes', $stored_hashes);

      // The plugin should no longer be ignored if was ignored only temporarily.
      if (!empty($ignored_plugins[$plugin_id]['temporary'])) {
        unset($ignored_plugins[$plugin_id]);
        $this->state->set('audit_report.ignored_plugins', $ignored_plugins);
      }
    }

    // Keep reporting ignored plugins, but mark them non-threatening.
    if (isset($ignored_plugins[$plugin_id])) {
      $result['severity'] = AuditCheckInterface::AUDIT_INFO;
      $result['ignored'] = $ignored_plugins[$plugin_id];
    }

    return $result;
  }

  /**
   * Executes the provided audit checks, or any active ones.
   *
   * @return array
   *   Array of audit results as for ::execute, keyed by plugin id.
   *
   * @todo Add optional "reset cache" flag?
   */
  public function executeMultiple(array $plugins = NULL) {
    $results = [];
    foreach ($plugins ?: $this->getActivePlugins() as $plugin_id => $plugin) {
      $results[$plugin_id] = $this->execute($plugin);
    }
    return $results;
  }

  /**
   * Generates a hash of the plugin's result.
   *
   * @param \Drupal\Core\Executable\ExecutableInterface $plugin
   *   The plugin to check.
   *
   * @return string
   *   The generated hash.
   */
  public function getResultHash(ExecutableInterface $plugin) {
    return $this->execute($plugin)['hash'];
  }

  /**
   * Generates a hash of the plugin's result.
   *
   * @param array $plugin_result
   *   The result array as returned from the plugin.
   *
   * @return string
   *   The generated hash.
   */
  protected function generateResultHash(array $plugin_result) {
    $hash_string = serialize([
      $plugin_result['value'] ?? '',
      $plugin_result['description'] ?? '',
    ]);
    return $plugin_result['severity'] . ':' . Crypt::hashBase64($hash_string);
  }

}
