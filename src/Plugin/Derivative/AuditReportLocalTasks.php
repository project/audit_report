<?php

namespace Drupal\audit_report\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\audit_report\Plugin\AuditCheckManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generates local tasks for audit check groups.
 */
class AuditReportLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The audit check manager.
   *
   * @var \Drupal\audit_report\Plugin\AuditCheckManager
   */
  protected $manager;

  /**
   * Creates an AuditReportLocalTasks object.
   *
   * @param \Drupal\audit_report\Plugin\AuditCheckManager $audit_check_manager
   *   The audit check manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation manager.
   */
  public function __construct(AuditCheckManager $audit_check_manager, TranslationInterface $string_translation) {
    $this->manager = $audit_check_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.audit_check'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    foreach ($this->manager->getAllTags() as $machine_name => $label) {
      $this->derivatives[$machine_name] = [
        'title' => $label,
        'route_parameters' => [
          'group' => $machine_name,
        ],
      ] + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
