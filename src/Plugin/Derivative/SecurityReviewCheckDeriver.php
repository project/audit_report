<?php

namespace Drupal\audit_report\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\security_review\Checklist;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generates audit checks for the security_review module.
 */
class SecurityReviewCheckDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The security_review checklist.
   *
   * @var \Drupal\security_review\Checklist
   */
  protected $checklist;

  /**
   * Instantiates the security_review audit check deriver object.
   *
   * @param \Drupal\security_review\Checklist|null $security_review_checklist
   *   The security_review checklist, if present.
   */
  public function __construct(Checklist $security_review_checklist = NULL) {
    $this->checklist = $security_review_checklist;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static($container->get('security_review.checklist', ContainerInterface::NULL_ON_INVALID_REFERENCE));
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    if (!$this->checklist) {
      // Service is not available, module likely not installed.
      return $this->derivatives;
    }

    foreach ($this->checklist->getChecks() as $check) {
      $this->derivatives[$check->id()] = [
        'label' => $check->getTitle(),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
