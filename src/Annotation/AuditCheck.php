<?php

namespace Drupal\audit_report\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Audit check item annotation object.
 *
 * @see \Drupal\audit_report\Plugin\AuditCheckManager
 * @see plugin_api
 *
 * @Annotation
 */
class AuditCheck extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The category of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category;

  /**
   * List of modules that need to be enabled for this plugin.
   *
   * @var string[]
   */
  public $modules;

  /**
   * List of tags associated with the audit check.
   *
   * @var string[]
   */
  public $tags;

}
