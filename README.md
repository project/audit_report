CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The Audit Report module provides an administrative overview of things you
might want to audit on your site.

This can be anything as simple as "module X is installed" to more complex
compliance checks for your organization or client. The underlying data
structure is flexible and pluggable and allows easilying extend the module with
additional checks.
Some audit checks are provided by submodules, but generally the module is
intended to be used with audit checks provided by other modules.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/audit_report

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/audit_report


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


RECOMMENDED MODULES
-------------------

Other modules that provide audit checks of their own:

 * [Security Review](https://www.drupal.org/project/security_review):
   The Security Review module automates testing for many of the easy-to-make
   mistakes that render your site insecure.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

 * You can view your audit report on `/admin/reports/status/audit`.


CONFIGURATION
-------------

 * Disable and re-enable audit checkts on Administration » Reports »
   Status report » Audit report.


MAINTAINERS
-----------

Current maintainers:
 * Patrick Fey (FeyP) - https://www.drupal.org/user/998680
 * David I. (Mirroar) - https://www.drupal.org/user/1533592
 * Anne (ckaotik) - https://www.drupal.org/user/2253676

This project has been sponsored by:
 * werk21 GmbH
   werk21 is a full service agency from Berlin, Germany, for politics,
   government, organizations and NGOs. Together with its customers,
   werk21 has realized many Drupal web sites from version 5 onwards.
   Visit http://www.werk21.de for more information.
